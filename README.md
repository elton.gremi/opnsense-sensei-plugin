![](https://static.wixstatic.com/media/a22e7f_3c6d0cd0f9e14cc9ba1e6d3b03e93f7a~mv2.jpg/v1/fill/w_184,h_42,al_c,q_80,usm_0.66_1.00_0.01/a22e7f_3c6d0cd0f9e14cc9ba1e6d3b03e93f7a~mv2.webp)

# Sensei - OPNsense Plugin

Sensei is a plugin for firewalls which complement them with features like Application Filtering, Advanced Network Visibility and Cloud Application Control. Currently, Sensei community edition is available for OPNsense platform.

##### Product Web Page:
https://sunnyvalley.io/sensei

## Features

##### 1. Application Control & Filtering
You can block and control unauthorized or misbehaving applications with Application Control. Sensei’s rich application database identifies thousands of applications.

##### 2. Advanced Network Visibility with Drill-Down Features
Visualize your network in real-time. Drill-down to per-connection details. Spot anomalies visually as they occur.

##### 3. All Ports Full TLS Inspection
Do not let malign network traffic abuse encryption and sneak behind your corperation. Sensei 
enables 100% transparent TLS inspection for all TCP ports with a single click. It is fully integrated with the OPNsense Firewall Certificate Manager.

##### 4. Web Filtering & Security with 140.000.000 categorized web sites.
Apply Web Security policies for more than 140 Million web sites under 120 different categories. Create custom categories to blacklist or whitelist sites.

##### 5. Cloud Application Control & Filtering
Using Cloud Application Control, easily create granular access policies for Cloud services like Google, Facebook, Dropbox and many more.

##### 6. Cloud Thread Intelligence
Realize the power of Cloud threat intelligence. Block zero-day malware and phishing attacks in real-time. Detect new Botnets in an instant.

## Editions
### Community Edition
This is a free edition for the community. This edition will be forever free.
### Enterprise Edition
Enterprise edition will be suitable for more advanced use cases. More information will be provided here once it has become generally available.

## How do I get Sensei?
Please sign-up on the product web page: [https://sunnyvalley.io/sensei](https://sunnyvalley.io/sensei) to get a download link. All you need to have is a valid e-mail address.

## How to install?
Once you have the download link, follow these three steps to install Sensei on your firewall:
- Installing Sensei on OPNsense firewall: [https://www.sunnyvalley.io/blog/installing-sensei-on-opnsense](https://www.sunnyvalley.io/blog/installing-sensei-on-opnsense)

You might also want to have a look at:
- Hardware Sizing Guide:
[https://www.sunnyvalley.io/blog/sensei-hw-requirements](https://www.sunnyvalley.io/blog/sensei-hw-requirements)
- Frequently Asked Questions: [FAQ](https://www.sunnyvalley.io/sensei)

 More on Sensei tutorials and live demonstrations: [Sunny Valley Networks Youtube Channel](https://www.youtube.com/channel/UCBmMJAnuUW5qxAN23kLPuPA)

## Getting Support
You can file a bug report [here at the gitlab project](https://gitlab.com/svn-community/opnsense-sensei-plugin/issues) if you encountered any issues while installing and using Sensei. Please provide OPNsense version, Sensei version and any other information that could help us to resolve the issue.

## Keep in Touch
- Follow us on Twitter: [https://twitter.com/sunnyvalley](https://twitter.com/sunnyvalley)
- Sunny Valley Networks Blog: [https://sunnyvalley.io/blog](https://sunnyvalley.io/blog)
- Subscribe to our [Youtube Channel](https://www.youtube.com/channel/UCBmMJAnuUW5qxAN23kLPuPA)

