#!/bin/sh

echo "Installation started..."

BASE_DIR="$(pwd)"
INSTALL_DIR="/usr/local"

echo "Removing legacy source codes..."
rm -rf $BASE_DIR/src/opnsense/mvc/app/controllers/OPNsense/Eastpect
rm -rf $BASE_DIR/src/opnsense/mvc/app/models/OPNsense/Eastpect
rm -rf $BASE_DIR/src/opnsense/mvc/app/views/OPNsense/Eastpect
rm -rf $BASE_DIR/src/opnsense/scripts/OPNsense/Eastpect
rm -rf $BASE_DIR/src/opnsense/service/templates/OPNsense/Eastpect
rm -rf $BASE_DIR/src/opnsense/www/js/eastpect
rm -f $BASE_DIR/src/opnsense/service/conf/actions.d/actions_eastpect.conf
rm -f $BASE_DIR/src/opnsense/www/css/eastpect.css
find $INSTALL_DIR/opnsense -iname "*eastpect*" -type d -exec echo "Deleting directory: {}" \; -prune -exec rm -rf "{}" \;
find $INSTALL_DIR/opnsense -iname "*eastpect*" -type f -exec echo "Deleting file: {}" \; -exec rm -f "{}" \;
rm -rf $INSTALL_DIR/opnsense/mvc/app/models/OPNsense/Sensei/Migrations
rm -f /opt/eastpect/scripts/installers/opnsense/18.1/web/uninstall.sh

echo "Copying plugin files..."
cp -rf $BASE_DIR/src/opnsense $INSTALL_DIR

echo "Invalidating OPNsense cache..."
rm -f $INSTALL_DIR/opnsense/mvc/app/cache/*.php
rm -f /tmp/opnsense_menu_cache.xml
rm -f /tmp/configdmodelfield.data

echo "Making Sensei scripts executable..."
chmod a+x $INSTALL_DIR/opnsense/scripts/OPNsense/Sensei/*

echo "Running OPNsense migration scripts..."
/usr/local/opnsense/mvc/script/run_migrations.php OPNsense/Sensei

echo "Running Sensei migration scripts..."
/usr/local/bin/php /usr/local/opnsense/mvc/app/models/OPNsense/Sensei/CLI.php migrate

echo "Running OPNsense post install scripts..."
/usr/local/etc/rc.configure_plugins POST_INSTALL

echo "Generating configuration files..."
/usr/local/sbin/configctl template reload OPNsense/Sensei

echo "Installing python dependencies..."
easy_install --upgrade --quiet pyopenssl 2> /dev/null

echo "Invalidating Sensei auto update check cache..."
rm -f /tmp/sensei_updates.json

echo "Restarting web gui..."
/usr/local/sbin/configctl webgui restart

echo "Restarting configd service..."
/usr/local/etc/rc.d/configd restart

echo "Installation completed."
