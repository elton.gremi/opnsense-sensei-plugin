<?php
namespace OPNsense\Sensei\Api;

use \OPNsense\Base\ApiControllerBase;
use \OPNsense\Sensei\Sensei;

class RulesController extends ApiControllerBase
{
    public function appsAction()
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'GET') {
            $appNodes = $sensei->getNodeByReference('rules.apps')->getNodes();
            $categoryNodes = $sensei->getNodeByReference('rules.appcategories')->getNodes();
            $categories = [];
            foreach ($categoryNodes as $category) {
                $categories[$category['name']] = [
                    'name' => $category['name'],
                    'apps' => []
                ];
            }
            foreach ($appNodes as $uuid=>$app) {
                if ($app['web20'] == 'no') {
                    $categories[$app['category']]['apps'][$app['name']] = [
                        'uuid' => $uuid,
                        'name' => $app['name'],
                        'web20' => $app['web20'],
                        'description' => $app['description'],
                        'action' => $app['action']
                    ];
                }
            }
            uksort($categories, 'strcasecmp');
            foreach ($categories as $key=>$category) {
                usort($categories[$key]['apps'], function($a, $b){
                     return strcmp($a['name'], $b['name']);
                });
            }
            return array_values($categories);
        } elseif ($this->request->getMethod() == 'POST') {
            $apps = $this->request->getPost('apps');
            foreach ($apps as $app) {
                $sensei->getNodeByReference('rules.apps.'.$app['uuid'])->setNodes([
                    'action' => $app['action']
                ]);
            }
            $this->configureApps($sensei);
            $sensei->saveChanges();
            return 'OK';
        }
    }

    public function webAction()
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'GET') {
            $webCategoryNodes = $sensei->getNodeByReference('rules.webcategories')->getNodes();
            $webCategories = [];
            foreach ($webCategoryNodes as $uuid=>$node) {
                if ($node['security'] == 'no') {
                    array_push($webCategories, [
                        'uuid' => $uuid,
                        'name' => $node['name'],
                        'action' => $node['action']
                    ]);
                }
            }
            usort($webCategories, function($a, $b) {
                return strcmp($a['name'], $b['name']);
            });
            return $webCategories;
        } elseif ($this->request->getMethod() == 'POST') {
            $filters = $this->request->getPost('filters');
            foreach ($filters as $filter) {
                $sensei->getNodeByReference('rules.webcategories.'.$filter['uuid'])->setNodes([
                    'action' => $filter['action']
                ]);
            }
            $sensei->saveChanges();
            return 'OK';
        }
    }

    public function web20Action()
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'GET') {
            $appNodes = $sensei->getNodeByReference('rules.apps')->getNodes();
            $web20Apps = [];
            foreach ($appNodes as $uuid=>$node) {
                if ($node['web20'] != 'no') {
                    if (!array_key_exists($node['web20'], $web20Apps)) {
                        $web20Apps[$node['web20']] = [
                            'name' => $node['web20'],
                            'apps' => []
                        ];
                    }
                    array_push($web20Apps[$node['web20']]['apps'], [
                        'uuid' => $uuid,
                        'name' => $node['name'],
                        'action' => $node['action']
                    ]);
                }
            }
            ksort($web20Apps);
            return array_values($web20Apps);
        } elseif ($this->request->getMethod() == 'POST') {
            $apps = $this->request->getPost('apps');
            foreach ($apps as $app) {
                $sensei->getNodeByReference('rules.apps.'.$app['uuid'])->setNodes([
                    'action' => $app['action']
                ]);
            }
            $this->configureApps($sensei);
            $sensei->saveChanges();
            return 'OK';
        }
    }

    public function securityAction()
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'GET') {
            $webCategoryNodes = $sensei->getNodeByReference('rules.webcategories')->getNodes();
            $securityCategories = [];
            foreach ($webCategoryNodes as $uuid=>$node) {
                if ($node['security'] == 'yes') {
                    array_push($securityCategories, [
                        'uuid' => $uuid,
                        'name' => $node['name'],
                        'action' => $node['action']
                    ]);
                }
            }
            usort($securityCategories, function($a, $b) {
                return strcmp($a['name'], $b['name']);
            });
            return $securityCategories;
        } elseif ($this->request->getMethod() == 'POST') {
            $rules = $this->request->getPost('rules');
            foreach ($rules as $rule) {
                $sensei->getNodeByReference('rules.webcategories.'.$rule['uuid'])->setNodes([
                    'action' => $rule['action']
                ]);
            }
            $sensei->saveChanges();
            return 'OK';
        }
    }

    private function configureApps($sensei)
    {
        $apps = $sensei->getNodeByReference('rules.apps')->getNodes();
        $appcategories = $sensei->getNodeByReference('rules.appcategories')->getNodes();
        $category_counts = [];
        $category_actions = [];
        foreach ($appcategories as $category) {
            $category_counts[$category['name']] = [
                'accept' => 0,
                'reject' => 0
            ];
        }
        foreach ($apps as $app) {
            $category_counts[$app['category']][$app['action']]++;
        }
        foreach ($appcategories as $uuid=>$category) {
            if ($category_counts[$category['name']]['reject'] > $category_counts[$category['name']]['accept']) {
                $sensei->getNodeByReference('rules.appcategories.' . $uuid)->setNodes([
                    'action' => 'reject',
                    'writetofile' => 'yes'
                ]);
            } else {
                $sensei->getNodeByReference('rules.appcategories.' . $uuid)->setNodes([
                    'action' => 'accept',
                    'writetofile' => 'no'
                ]);
            }
            $category_actions[$category['name']] = $category['action'];
        }
        foreach ($apps as $uuid=>$app) {
            $rejects_less = $category_counts[$app['category']]['reject'] <= $category_counts[$app['category']]['accept'];
            $rejects_more = $category_counts[$app['category']]['reject'] > $category_counts[$app['category']]['accept'];
            if (($rejects_less and $app['action'] == 'reject') or ($rejects_more and $app['action'] == 'accept')) {
                $sensei->getNodeByReference('rules.apps.' . $uuid)->setNodes([
                    'writetofile' => 'yes'
                ]);
            } else {
                $sensei->getNodeByReference('rules.apps.' . $uuid)->setNodes([
                    'writetofile' => 'no'
                ]);
            }
        }
    }

    public function customWebAction($uuid=null)
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'PUT') {
            $category = $this->request->getPut('category');
            $node = $sensei->rules->customwebcategories->Add();
            $node->setNodes([
                'name' => $category
            ]);
            $sensei->saveChanges();
            return 'OK';
        } elseif ($this->request->getMethod() == 'GET') {
            $categoryNodes = $sensei->getNodeByReference('rules.customwebcategories')->getNodes();
            $categories = [];
            foreach ($categoryNodes as $uuid => $node) {
                array_push($categories, [
                    'uuid' => $uuid,
                    'name' => $node['name'],
                    'action' => $node['action']
                ]);
            }
            usort($categories, function($a, $b) {
                 return strcmp($a['name'], $b['name']);
            });
            return $categories;
        } elseif ($this->request->getMethod() == 'POST') {
            if ($this->request->hasPost('filters')) {
                $filters = $this->request->getPost('filters');
                foreach ($filters as $filter) {
                    $sensei->getNodeByReference('rules.customwebcategories.'.$filter['uuid'])->setNodes([
                        'action' => $filter['action']
                    ]);
                }
                $sensei->saveChanges();
            }
            return 'OK';
        } elseif ($this->request->getMethod() == 'DELETE') {
            $sensei->getNodeByReference('rules.customwebcategories')->del($uuid);
            $ruleNodes = $sensei->getNodeByReference('rules.customwebrules')->getNodes();
            foreach ($ruleNodes as $ruleid=>$rule) {
                if ($rule['catid'] == $uuid) {
                    $sensei->getNodeByReference('rules.customwebrules')->del($ruleid);
                }
            }
            $sensei->saveChanges();
            return 'OK';
        }
    }

    public function customWebRuleAction($uuid=null)
    {
        $sensei = new Sensei();
        if ($this->request->getMethod() == 'DELETE') {
            $sensei->getNodeByReference('rules.customwebrules')->del($uuid);
            $sensei->saveChanges();
            return 'OK';
        } elseif ($this->request->getMethod() == 'GET') {
            $rules = [];
            $ruleNodes = $sensei->getNodeByReference('rules.customwebrules')->getNodes();
            foreach ($ruleNodes as $ruleid=>$rule) {
                if ($rule['catid'] == $uuid) {
                    array_push($rules, [
                        'uuid' => $ruleid,
                        'site' => $rule['site']
                    ]);
                }
            }
            usort($rules, function($a, $b){
                 return strcmp($a['site'], $b['site']);
            });
            return $rules;
        } elseif ($this->request->getMethod() == 'PUT') {
            $rule = $this->request->getPut('rule');
            $category = $sensei->getNodeByReference('rules.customwebcategories.' . $uuid)->getNodes()['name'];
            $node = $sensei->rules->customwebrules->Add();
            $node->setNodes([
                'site' => $rule,
                'category' => $category,
                'catid' => $uuid
            ]);
            $sensei->saveChanges();
            return 'OK';
        }
    }
}
