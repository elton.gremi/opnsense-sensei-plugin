<?php
namespace OPNsense\Sensei;

use \OPNsense\Base\BaseModel;
use \OPNsense\Core\Config;
use \OPNsense\Core\Backend;

class Sensei extends BaseModel
{
    public $tlsWhitelistFileDefault = '/opt/eastpect/policy/Rules/tlswhitelist.rules.default';
    public $tlsWhitelistFile = '/opt/eastpect/policy/Rules/tlswhitelist.rules';
    public $certFile = '/opt/eastpect/cert/ca/rootCA.pem';
    public $certKeyFile = '/opt/eastpect/cert/ca/rootCA.key';
    public $landingPage = '/opt/eastpect/userdefined/etc/landing.html';
    public $configDoneFile = '/opt/eastpect/etc/.configdone';
    public $webControlsJson = '/opt/eastpect/db/webui/webcats.json';
    public $appControlsJson = '/opt/eastpect/db/webui/apps.json';
    public $cloudReputationServersFile = '/opt/eastpect/db/Cloud/nodes.csv';
    public $uninstallScript = '/usr/local/opnsense/scripts/OPNsense/Sensei/uninstall_engine.sh';
    public $updatesJson = '/tmp/sensei_updates.json';

    public function saveChanges()
    {
        $this->serializeToConfig();
        return Config::getInstance()->save();
    }

    public function reloadEngine($section=null)
    {
        $response = [];
        if (!$this->engineIsRunning()) {
            $response['message'] = 'Engine is not running';
            return $response;
        }
        $commands = $section ? ["reload ".$section."\n"] : ["reload db\n", "reload rules\n"];
        $interfaceNodes = $this->getNodeByReference('interfaces')->getNodes();
        foreach ($interfaceNodes as $uuid => $node) {
            $response = $this->runTelnetCommands(intval($node['manageport']), $commands, $response);
        }
        return $response;
    }

    private function engineIsRunning()
    {
        $backend = new Backend();
        return strpos($backend->configdRun('sensei service eastpect status'), 'is running') !== false;
    }

    private function runTelnetCommands($port, $commands, $response)
    {
        $backend = new Backend();
        $result = $backend->configdRun('sensei service eastpect status');
        if (strpos($result, 'not running') !== false) {
            $response['message'] = 'Sensei service is not running';
            return $response;
        }
        $enrich = $this->getNodeByReference('enrich')->getNodes();
        $tcpServiceEnabled = $enrich['tcpServiceEnable'];
        if ($tcpServiceEnabled == 'true') {
            $tcpServiceIP = $enrich['tcpServiceIP'];
            $tcpServicePsk = $enrich['tcpServicePsk'];
            $socket = fsockopen($tcpServiceIP, $port, $errno, $errstr, 30);
            if ($socket) {
                $response['outputs'] = [];
                fputs($socket, "pass ".$tcpServicePsk."\n");
                sleep(3);
                array_push($response['outputs'], fgets($socket, 1024));
                foreach ($commands as $command) {
                    fputs($socket, $command);
                    sleep(3);
                    array_push($response['outputs'], fgets($socket, 1024));
                }
                fclose($socket);
                return $response;
            } else {
                $response['message'] = 'TCP service connection error';
                return $response;
            }
        } else {
            $response['message'] = 'TCP service disabled';
            return $response;
        }
    }

    public function checkUpdates($auto=true, $section=null)
    {
        $backend = new Backend();
        $updates = [];
        $response = [];
        if ($auto) {
            if (file_exists($this->updatesJson)) {
                $updatesJson = json_decode(file_get_contents($this->updatesJson));
            } else {
                $updatesJson = json_decode($backend->configdRun('sensei check-updates'));
            }
        } else {
            $updatesJson = json_decode($backend->configdRun('sensei check-updates'));
        }
        if (isset($updatesJson)) {
            if (!$section || $section == 'engine') {
                if (isset($updatesJson->engine->update_required) && $updatesJson->engine->update_required) {
                    array_push($updates, 'Engine:'.implode(',', $updatesJson->engine->versions_available));
                }
            }
            if ((!$section || $section == 'database') && count($updates) == 0) {
                if (isset($updatesJson->database->update_required) && $updatesJson->database->update_required) {
                    array_push($updates, 'Database:'.$updatesJson->database->compatible_latest_version);
                }
            }
            $this->setNodes([
                'updater' => [
                    'lastupdate' => (string) time()
                ]
            ]);
            $this->saveChanges();
        }
        foreach ($updates as $update) {
            if ($update) {
                $updateArr = explode(':', $update);
                $versionArr = explode(',', $updateArr[1]);
                foreach ($versionArr as $version) {
                    if ($version) {
                        array_push($response, [
                            'section' => $updateArr[0],
                            'version' => $version
                        ]);
                    }
                }
            }
        }
        return $response;
    }
}
