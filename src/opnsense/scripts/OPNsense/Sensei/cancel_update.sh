#!/bin/sh

PID_FILE="/tmp/sensei_download.pid"

if [ ! -f "$PID_FILE" ]
then
    echo "Can not find a download progress."
    exit 0
fi

PID="$(cat $PID_FILE)"

PID_EXISTS="$(ps aux | grep $PID | grep -v "grep")"

if [ -z "$PID_EXISTS" ]
then
    echo "Can not find a download progress."
    exit 0
fi

kill -9 $PID

echo "Update canceled."

exit 0
