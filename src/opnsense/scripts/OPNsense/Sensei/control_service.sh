#!/bin/sh

# usage: ./control_service.sh <service> <action>
# usage: ./control_service.sh eastpect start

if [ "$1" == "elasticsearch" -a "$2" == "start" -a -z "$(df | grep /proc)" ]; then
    mount -t fdescfs fdesc "/dev/fd" 2>/dev/null
    mount -t procfs proc "/proc" 2>/dev/null
fi

if [ "$1" == "eastpect" ]; then
    /opt/eastpect/scripts/service.sh $2 2>/dev/null
else
    service $1 one$2 2>/dev/null
fi

if [ "$1" == "elasticsearch" -a "$2" == "stop" -a -n "$(df | grep /proc)" ]; then
    umount "/dev/fd" 2>/dev/null
    umount "/proc" 2>/dev/null
fi

exit 0
